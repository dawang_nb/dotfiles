#!/bin/bash

if [ -n "$CODER" ]; then
    DOTFILE_PATH=$HOME/.config/coderv2/dotfiles
else
    DOTFILE_PATH=$HOME/.dotfiles
    mv $HOME/.gitconfig $HOME/.gitconfig.bak
fi

ln -s $DOTFILE_PATH/.gitconfig $HOME/.gitconfig
ln -s $DOTFILE_PATH/.bash_aliases $HOME/.bash_aliases
cp $DOTFILE_PATH/e_hooks.sh $HOME/e_hooks
cp $DOTFILE_PATH/r_hooks.sh $HOME/r_hooks
echo "source $DOTFILE_PATH/dbt.env" >> $HOME/.bashrc

chmod -R 777 ~/.aws