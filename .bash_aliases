alias tf=terraform
alias dcr='docker-compose run'
alias dcra='docker-compose run -e DISABLE_SPRING=true api bundle exec'
alias b2m="git checkout master && git pull --rebase origin master"
alias gpo="git push origin"
alias gpfo="git push -f origin"
alias grm="git rebase master"
alias gm="git commit -m"
alias gam="git commit -am"
alias gcb="git checkout -b"
alias gcm="git checkout master"
alias gco="git checkout"
alias gs="git status"
alias switch-to-staging="aws --profile staging-eks eks update-kubeconfig --name snaptravel-staging-eks"
alias switch-to-production="aws --profile production-eks eks update-kubeconfig --name snaptravel-production-eks"
alias switch-to-gitlab="aws --profile gitlab-eks eks update-kubeconfig --name gitlab-runner-cluster"

alias linter="bash /home/coder/.config/coderv2/dotfiles/linter"
alias linter-mod="bash /home/coder/.config/coderv2/dotfiles/linter-mod"
alias linter-folder="bash /home/coder/.config/coderv2/dotfiles/linter-folder"
alias dc="$(docker ps | grep "scheduler" | awk -F " " '{print $1}' | xargs -i echo "docker exec -it {} /bin/bash")"
alias ab="alembic -c /workspace/datapipeline/alembic/hotel_catalog/alembic.ini"
alias awss="aws sso login ;sudo chmod -R 777 ~/.aws"
echo "DA_DOT_FILES"
